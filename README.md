# Test Application

## Description 
### Task 1
Simple project to collect countries and numbers available in country from Onlinesim API. 

### Task 2
Simple project to parse web page to collect prices by countries and serialize it to JSON;

## Run
To run application use command `./gradlew run`


# Task 3

## Given
Refactor the following method
```java
void processTask(ChannelHandlerContext ctx) { 
    InetSocketAddress lineAddress = new InetSocketAddress(getIpAddress(), getUdpPort());  CommandType typeToRemove; 
    for (Command currentCommand : getAllCommands()) { 
        if (currentCommand.getCommandType() == CommandType.REBOOT_CHANNEL) {  
            if (!currentCommand.isAttemptsNumberExhausted()) { 
                if (currentCommand.isTimeToSend()) { 
                    sendCommandToContext(ctx, lineAddress, currentCommand.getCommandText()); 
                    try { 
                        AdminController.getInstance().processUssdMessage( 
                        new DblIncomeUssdMessage(lineAddress.getHostName(), lineAddress.getPort(), 0,  EnumGoip.getByModel(getGoipModel()), currentCommand.getCommandText()), false);  
                    } catch (Exception ignored) { } 
                    
                    currentCommand.setSendDate(new Date()); 
                    Log.ussd.write(String.format("sent: ip: %s; порт: %d; %s", 
                    lineAddress.getHostString(), lineAddress.getPort(), currentCommand.getCommandText()));  currentCommand.incSendCounter(); 
                } 
            } else { 
                typeToRemove = currentCommand.getCommandType(); 
                deleteCommand(typeToRemove); 
            } 
        } else { 
            if (!currentCommand.isAttemptsNumberExhausted()) { 
                sendCommandToContext(ctx, lineAddress, currentCommand.getCommandText()); 

                try { 
                    AdminController.getInstance().processUssdMessage( 
                        new DblIncomeUssdMessage(lineAddress.getHostName(), lineAddress.getPort(), 0,  EnumGoip.getByModel(getGoipModel()), currentCommand.getCommandText()), false);  
                } catch (Exception ignored) { } 
                
                Log.ussd.write(String.format("sent: ip: %s; порт: %d; %s", 
                lineAddress.getHostString(), lineAddress.getPort(), currentCommand.getCommandText()));  currentCommand.setSendDate(new Date()); 
                currentCommand.incSendCounter(); 
            } else { 
                typeToRemove = currentCommand.getCommandType(); 
                deleteCommand(typeToRemove); 
            }
        } 
    } 
    sendKeepAliveOkAndFlush(ctx);  
}
```

## Result
```java
void processTask(ChannelHandlerContext ctx) { 
    InetSocketAddress lineAddress = new InetSocketAddress(getIpAddress(), getUdpPort());  CommandType typeToRemove; 
    
    for (Command currentCommand : getAllCommands()) { 
        checkAndProcessCommand(lineAddress, currentCommand)
    } 
    sendKeepAliveOkAndFlush(ctx);  
}

void checkAndProcessCommand(InetSocketAddress lineAddress, Command currentCommand) {
    if (!currentCommand.isAttemptsNumberExhausted()) {
        if (currentCommand.getCommandType() == CommandType.REBOOT_CHANNEL) {  
            if (currentCommand.isTimeToSend()) { 
                processCommand(lineAddress, currentCommand);
            } 
        } else { 
            processCommand(lineAddress, currentCommand);
        } 
    } else {
        deleteCommand(currentCommand.getCommandType());
    }
}

void processCommand(InetSocketAddress lineAddress, Command currentCommand) {
    sendCommandToContext(ctx, lineAddress, currentCommand.getCommandText()); 
    try { 
        AdminController.getInstance().processUssdMessage( 
        new DblIncomeUssdMessage(lineAddress.getHostName(), lineAddress.getPort(), 0,  EnumGoip.getByModel(getGoipModel()), currentCommand.getCommandText()), false);  
    } catch (Exception ignored) {
        Log.ussd.write(String.format("ERROR while sending: ip: %s; порт: %d; %s", lineAddress.getHostString(), lineAddress.getPort(), currentCommand.getCommandText()));  
    } 

    currentCommand.setSendDate(new Date()); 
    currentCommand.incSendCounter(); 
    
    Log.ussd.write(String.format("sent: ip: %s; порт: %d; %s", lineAddress.getHostString(), lineAddress.getPort(), currentCommand.getCommandText()));  
}
```
