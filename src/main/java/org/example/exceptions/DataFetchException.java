package org.example.exceptions;

public class DataFetchException extends Exception {
    private final String url;

    public DataFetchException(String url, String message) {
        super(message);
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
