package org.example;

import java.util.Scanner;
import org.example.service.OSimService;
import org.example.service.impl.OSimServiceImpl;

public class Main {
    private static final OSimService oSimService = new OSimServiceImpl();

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Please enter task number to run: \n" +
            " * Enter \"1\" to Collect countries and numbers \n" +
            " * Enter \"2\" to Parse prices");

        String taskNumber = in.nextLine();
        if(taskNumber.equals("1")) {
            runTask1();
        } else if (taskNumber.equals("2")) {
            runTask2();
        } else {
            System.out.println("Inconsistent task number been entered. Relaunch and Try again.");
        }
    }

    public static void runTask1() {
        oSimService.collectCountries();
        oSimService.collectNumbers(oSimService.getAllCountries());

        oSimService.getAllCountries().forEach(System.out::println);
    }

    public static void runTask2() {
        System.out.println("Please, be patient, fetching prices will take some time");
        System.out.println(oSimService.collectPrices());
    }
}