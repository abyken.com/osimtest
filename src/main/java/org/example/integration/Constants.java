package org.example.integration;

public class Constants {
    public static String OSIM_COUNTRIES_URL="https://onlinesim.ru/api/getFreeCountryList";
    public static String OSIM_NUMBERS_URL="https://onlinesim.ru/api/getFreePhoneList?country=%d";

    public static String OSIM_PRICES_URL="https://onlinesim.io/price-list";
}
