package org.example.integration.impl;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.function.Supplier;
import org.example.exceptions.DataFetchException;
import org.example.integration.Constants;
import org.example.integration.OnlinesimIntegration;
import org.example.integration.dto.OSimCountriesResponseWrapper;
import org.example.integration.dto.OSimCountry;
import org.example.integration.dto.OSimNumbers;
import java.util.List;
import org.example.integration.dto.OSimNumbersResponseWrapper;
import org.example.integration.handlers.JsonBodyHandler;

public class OnlinesimIntegrationImpl implements OnlinesimIntegration {
    private final HttpClient httpClient;

    public OnlinesimIntegrationImpl() {
        this.httpClient = HttpClient.newHttpClient();
    }

    @Override
    public List<OSimCountry> getFreeCountryList() throws IOException, InterruptedException, DataFetchException {
        HttpResponse<Supplier<OSimCountriesResponseWrapper>> response = this.httpClient.send(
            this.buildRequest(Constants.OSIM_COUNTRIES_URL), new JsonBodyHandler<>(OSimCountriesResponseWrapper.class));

        if (response.statusCode() != 200 && response.body().get().getResponse() != 1) {
            throw new DataFetchException(Constants.OSIM_COUNTRIES_URL, "Fetching all countries exception");
        }

        return response.body().get().getCountries();
    }
    @Override
    public List<OSimNumbers> getFreePhoneList(Integer countryId) throws IOException, InterruptedException, DataFetchException {
        String url = String.format(Constants.OSIM_NUMBERS_URL, countryId);
        HttpResponse<Supplier<OSimNumbersResponseWrapper>> response = this.httpClient.send(
            this.buildRequest(url), new JsonBodyHandler<>(OSimNumbersResponseWrapper.class));

        if (response.statusCode() != 200 && !response.body().get().getResponse().equals("1")) {
            throw new DataFetchException(url, "Fetching numbers exception");

        }

        return response.body().get().getNumbers();
    }

    private HttpRequest buildRequest(String url) {
        return HttpRequest.newBuilder(URI.create(url))
            .header("accept", "application/json")
            .build();
    }
}
