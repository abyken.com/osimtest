package org.example.integration.impl;

import static org.example.integration.Constants.OSIM_PRICES_URL;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.example.integration.OnlinesimPricesIntegration;

public class OnlinesimPricesIntegrationImpl implements OnlinesimPricesIntegration {
    private final WebClient client;

    public OnlinesimPricesIntegrationImpl() {
        client = new WebClient();
        client.getOptions().setCssEnabled(false);
        client.getOptions().setJavaScriptEnabled(true);
        client.getOptions().setThrowExceptionOnScriptError(false);
    }

    @Override
    public Map<String, Map<String, Map<String, Float>>> getPrices() throws IOException {
        Map<String, Map<String, Map<String, Float>>> result = new HashMap<>();
        result.put("receive", this.collectCountriesPrices("receive"));
        result.put("rent", this.collectCountriesPrices("rent"));
        result.put("proxy", this.collectCountriesPrices("proxy"));
        return result;
    }

    private Map<String, Map<String, Float>> collectCountriesPrices(String type) throws IOException {
        Map<String, Map<String, Float>> countries = new HashMap<>();
        HtmlPage page = this.fetchPage(OSIM_PRICES_URL+"?type=" + type);
        List<HtmlElement> items = page.getByXPath("//div[contains(@class, 'country-block')]/a[contains(@class, 'btn-block')]");
        for (HtmlElement item : items) {
            HtmlElement spanCountry = ((HtmlElement) item.getFirstByXPath("./span")) ;
            countries.put(spanCountry.getTextContent(), this.getServicePrices(type, parseCountryId(item.getAttribute("id"))));
        }

        return countries;
    }

    private Map<String, Float> getServicePrices(String type, String id) throws IOException {
        Map<String, Float> prices = new HashMap<>();
        HtmlPage page = fetchPage(OSIM_PRICES_URL+"?type=" + type + "&country="+id);
        List<HtmlElement> items = page.getByXPath("//div[contains(@class, 'service-block')]");

        for (HtmlElement item : items) {
            HtmlElement spanName = ((HtmlElement) item.getFirstByXPath("./span[@class='price-name']"));
            HtmlElement spanText = ((HtmlElement) item.getFirstByXPath("./span[@class='price-text']"));
            prices.put(spanName.getTextContent(), Float.valueOf(spanText.getTextContent()));
        }

        return prices;
    }

    private String parseCountryId(String id) {
        String[] parts = id.split("-");
        return parts[parts.length - 1];
    }

    private HtmlPage fetchPage(String url) throws IOException {
        HtmlPage page =  client.getPage(url);
        this.client.waitForBackgroundJavaScript(5_000);
        return page;
    }
}
