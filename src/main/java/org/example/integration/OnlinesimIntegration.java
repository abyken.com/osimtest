package org.example.integration;

import java.io.IOException;
import java.net.MalformedURLException;
import org.example.exceptions.DataFetchException;
import org.example.integration.dto.OSimCountry;
import org.example.integration.dto.OSimNumbers;
import java.util.List;

public interface OnlinesimIntegration {
    List<OSimCountry> getFreeCountryList() throws IOException, InterruptedException, DataFetchException;
    List<OSimNumbers> getFreePhoneList(Integer countryId) throws IOException, InterruptedException, DataFetchException;
}
