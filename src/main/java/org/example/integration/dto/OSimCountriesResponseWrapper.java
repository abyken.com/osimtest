package org.example.integration.dto;

import java.util.List;

public class OSimCountriesResponseWrapper {
    private Integer response;
    private List<OSimCountry> countries;

    public OSimCountriesResponseWrapper() {
    }

    public OSimCountriesResponseWrapper(Integer response, List<OSimCountry> countries) {
        this.response = response;
        this.countries = countries;
    }

    public Integer getResponse() {
        return response;
    }

    public void setResponse(Integer response) {
        this.response = response;
    }

    public List<OSimCountry> getCountries() {
        return countries;
    }

    public void setCountries(List<OSimCountry> countries) {
        this.countries = countries;
    }
}
