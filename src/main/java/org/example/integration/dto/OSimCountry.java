package org.example.integration.dto;

import java.util.List;

public class OSimCountry {
    private Integer country;
    private String country_text;

    public List<OSimNumbers> getNumbers() {
        return numbers;
    }

    public void setNumbers(List<OSimNumbers> numbers) {
        this.numbers = numbers;
    }

    private List<OSimNumbers> numbers;

    public OSimCountry() {
    }

    public OSimCountry(Integer country, String country_text) {
        this.country = country;
        this.country_text = country_text;
    }

    public Integer getCountry() {
        return country;
    }

    public void setCountry(Integer country) {
        this.country = country;
    }

    public String getCountry_text() {
        return country_text;
    }

    public void setCountry_text(String country_text) {
        this.country_text = country_text;
    }

    @Override
    public String toString() {
        return "OSimCountry{" +
            "country=" + country +
            ", country_text='" + country_text + '\'' +
            ", numbers=" + numbers +
            '}';
    }
}
