package org.example.integration.dto;

import java.util.List;

public class OSimNumbersResponseWrapper {
    private String response;
    private List<OSimNumbers> numbers;

    public OSimNumbersResponseWrapper() {
    }

    public OSimNumbersResponseWrapper(String response, List<OSimNumbers> numbers) {
        this.response = response;
        this.numbers = numbers;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public List<OSimNumbers> getNumbers() {
        return numbers;
    }

    public void setNumbers(List<OSimNumbers> numbers) {
        this.numbers = numbers;
    }
}
