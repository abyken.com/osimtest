package org.example.integration;

import java.io.IOException;
import java.util.Map;

public interface OnlinesimPricesIntegration {
    Map<String, Map<String, Map<String, Float>>> getPrices() throws IOException;
}
