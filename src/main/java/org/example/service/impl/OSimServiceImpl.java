package org.example.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.example.integration.OnlinesimIntegration;
import org.example.integration.OnlinesimPricesIntegration;
import org.example.integration.dto.OSimCountry;
import org.example.integration.dto.OSimNumbers;
import org.example.integration.impl.OnlinesimIntegrationImpl;
import org.example.integration.impl.OnlinesimPricesIntegrationImpl;
import org.example.service.OSimService;

public class OSimServiceImpl implements OSimService {
    private List<OSimCountry> countries;
    private final OnlinesimIntegration integration;
    private final OnlinesimPricesIntegration pricesIntegration;

    public OSimServiceImpl() {
        integration = new OnlinesimIntegrationImpl();
        pricesIntegration = new OnlinesimPricesIntegrationImpl();
    }

    @Override
    public void collectCountries() {
        try {
            countries = integration.getFreeCountryList();
        } catch (Exception e) {
            System.out.println("Error while fetching country data");
        }
    }

    @Override
    public void collectNumbers(List<OSimCountry> countries) {
        countries.forEach(country -> {
            try {
                country.setNumbers(integration.getFreePhoneList(country.getCountry()));
            } catch (Exception e) {
                System.out.printf("Error while fetching numbers by country ID %d%n", country.getCountry());
            }
        });
    }

    @Override
    public List<OSimCountry> getAllCountries() {
        return this.countries;
    }

    @Override
    public List<OSimNumbers> getNumbersByCountry(Integer countryId) {
        OSimCountry country = this.getCountryById(countryId);
        return country != null ? country.getNumbers() : null;
    }

    @Override
    public OSimCountry getCountryById(Integer countryId) {
        Optional<OSimCountry> countryOptional = this.countries.stream()
            .filter(item -> countryId.equals(item.getCountry())).findFirst();

        return countryOptional.isEmpty() ? null : countryOptional.get();
    }

    @Override
    public String collectPrices() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(pricesIntegration.getPrices());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


}
