package org.example.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.example.integration.dto.OSimCountry;
import org.example.integration.dto.OSimNumbers;

public interface OSimService {
    void collectCountries();
    void collectNumbers(List<OSimCountry> countries);
    List<OSimCountry> getAllCountries();
    List<OSimNumbers> getNumbersByCountry(Integer countryId);

    OSimCountry getCountryById(Integer countryId);

    String collectPrices();
}
